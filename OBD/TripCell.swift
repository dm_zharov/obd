//
//  TripCell.swift
//  OBD
//
//  Created by Дмитрий Жаров on 02.05.17.
//  Copyright © 2017 Dmitriy Zharov. All rights reserved.
//

import UIKit
import MapKit

class TripCell: UITableViewCell {
    
    @IBOutlet var departureTimeLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    
    @IBOutlet var costLabel: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var lastingTimeLabel: UILabel!
    
    @IBOutlet var mapSnapshot: UIImageView!
    
    @IBOutlet var cardView: UIView!
    
    var departureLocation: CLLocationCoordinate2D!
    var arrivalLocation: CLLocationCoordinate2D!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let departurePlacemark = MKPlacemark(coordinate: departureLocation, addressDictionary: nil)
        let arrivalPlacemark = MKPlacemark(coordinate: arrivalLocation, addressDictionary: nil)
        
        let departureMapItem = MKMapItem(placemark: departurePlacemark)
        let arrivalMapItem = MKMapItem(placemark: arrivalPlacemark)
        
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = departureMapItem
        directionRequest.destination = arrivalMapItem
        directionRequest.transportType = .automobile
        directionRequest.requestsAlternateRoutes = false
        
        let directions = MKDirections(request: directionRequest)
        
        directions.calculate(completionHandler: {(response, error) in
            if error != nil {
                print("Error getting directions")
            } else {
                self.takeSnapshot(response!)
            }
        })
        
        cardView.layer.borderWidth = 0.5
        cardView.layer.borderColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:1.0).cgColor
    }
    
    func takeSnapshot(_ response: MKDirectionsResponse) {
        
        let mapSnapshotOptions = MKMapSnapshotOptions()
        
        // Set the region of the map that is rendered. (by polyline)
        let polyline = response.routes[0].polyline
        var region = MKCoordinateRegionForMapRect(polyline.boundingMapRect)
        region.span.latitudeDelta *= 1.25
        region.span.longitudeDelta *= 1.25
        
        mapSnapshotOptions.region = region
        
        // Set the scale of the image. We'll just use the scale of the current device, which is 2x scale on Retina screens.
        mapSnapshotOptions.scale = UIScreen.main.scale
        
        // Set the size of the image output.
        mapSnapshotOptions.size = CGSize(width: 375, height: 150)
        
        // Show buildings and Points of Interest on the snapshot
        mapSnapshotOptions.showsBuildings = true
        mapSnapshotOptions.showsPointsOfInterest = false
        
        let mapSnapshotter = MKMapSnapshotter(options: mapSnapshotOptions)
        
        mapSnapshotter.start() { snapshot, error in
            guard let snapshot = snapshot else {
                return
            }
            // Don't just pass snapshot.image, pass snapshot itself!
            self.mapSnapshot.image = self.drawLineOnImage(snapshot: snapshot, polyline: polyline)
        }
    }
    
    func drawLineOnImage(snapshot: MKMapSnapshot, polyline: MKPolyline) -> UIImage {
        // Snapshotter objects do not capture the visual representations of any overlays or annotations that your app creates. If you want those items to appear in the final snapshot, you must draw them on the resulting snapshot image. For more information about drawing custom content on map snapshots, see MKMapSnapshot.
        
        let image = snapshot.image
        let pin = #imageLiteral(resourceName: "Pin")
        
        
        // for Retina screen
        UIGraphicsBeginImageContextWithOptions(self.mapSnapshot.frame.size, true, 0)
        
        // draw original image into the context
        image.draw(at: CGPoint.zero)
        
        // get the context for CoreGraphics
        let context = UIGraphicsGetCurrentContext()
        
        // set stroking width and color of the context
        context!.setLineWidth(3.0)
        context!.setStrokeColor(UIColor(red:1.00, green:0.18, blue:0.33, alpha:1.0).cgColor)
        
        // Here is the trick :
        // We use addLine() and move() to draw the line, this should be easy to understand.
        // The diificult part is that they both take CGPoint as parameters, and it would be way too complex for us to calculate by ourselves
        // Thus we use snapshot.point() to save the pain.
        let pointCount = polyline.pointCount
        let pointCoordinates = UnsafeMutablePointer<CLLocationCoordinate2D>.allocate(capacity: polyline.pointCount)
        polyline.getCoordinates(pointCoordinates, range: NSMakeRange(0, pointCount))
        context!.move(to: snapshot.point(for: polyline.coordinate))
        for i in 0...polyline.pointCount-1 {
            
            if i == 0 {
                context!.move(to: snapshot.point(for: pointCoordinates[i]))
            } else {
                context!.addLine(to: snapshot.point(for: pointCoordinates[i]))
            }
        }
        
        var pinCoord: CGPoint
        pinCoord = snapshot.point(for: pointCoordinates[0])
        pinCoord.x -= #imageLiteral(resourceName: "Pin").size.width / 2
        pin.draw(at: pinCoord)
        pinCoord = snapshot.point(for: pointCoordinates[polyline.pointCount-1])
        pinCoord.y -= #imageLiteral(resourceName: "Pin").size.height / 2
        pin.draw(at: pinCoord)
        
        pointCoordinates.deallocate(capacity: polyline.pointCount)
        
        // apply the stroke to the context
        context!.strokePath()
        
        // get the image from the graphics context
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()
        
        // end the graphics context 
        UIGraphicsEndImageContext()
        
        return resultImage!
    }
}
