//
//  ErrorStore.swift
//  OBD
//
//  Created by Дмитрий Жаров on 03.05.17.
//  Copyright © 2017 Dmitriy Zharov. All rights reserved.
//

import UIKit

class ErrorStore {
    
    var allErrors = [Error]()
    
    /* @discardableResult func createError() -> Error {
        let newError = Error()
        
        allErrors.append(newError)
        
        return newError
    } */
    
    func removeError(_ item: Error) {
        if let index = allErrors.index(of: item) {
            allErrors.remove(at: index)
        }
    }
    
    init() {
        allErrors.append(Error(errorTitle: "P0100 - Check Fuel System", errorDesc: "Mass or Volume Air Flow Circuit", errorDate: "СРЕДА, 7 АПРЕЛЯ, 2017"))
        allErrors.append(Error(errorTitle: "P0200 - Powertrain", errorDesc: "Injector Circuit", errorDate: "СРЕДА, 5 АПРЕЛЯ, 2017"))
        
        // createTrip()
    }
}
