//
//  TripsStorage.swift
//  OBD
//
//  Created by Дмитрий Жаров on 02.05.17.
//  Copyright © 2017 Dmitriy Zharov. All rights reserved.
//

import UIKit

class TripStore {
    
    var allTrips = [Trip]()
    
    init() {
        for i in 0...Int(arc4random_uniform(UInt32(5))) {
            
            let backToTheFuture = TimeInterval(i * -60 * 60 * 24)
            
            var departureCoords: [(latitude: Double, longitude: Double)] = [(55.6697, 37.2793),
                                                                            (55.7560, 37.6207),
                                                                            (55.8091, 37.5263),
                                                                            (55.7410, 37.5788),
                                                                            (55.7821, 37.5713)]
            
            var arrivalCoords: [(latitude: Double, longitude: Double)] = [(55.8035, 37.4100),
                                                                          (55.7658, 37.7175),
                                                                          (55.7282, 37.6587),
                                                                          (55.7346, 37.6721),
                                                                          (55.8058, 37.7116)]
            
            allTrips.append(Trip(
                departureTime: Date(timeIntervalSinceNow: backToTheFuture),
                arrivalTime: Date(timeInterval: TimeInterval(Int(arc4random_uniform(UInt32(7200))) + 900), since: Date(timeIntervalSinceNow: backToTheFuture)),
                distance: Double(arc4random_uniform(UInt32(32500)) + 2500),
                fuelCons: Float(arc4random_uniform(UInt32(10)) + 5),
                fuelPrice: 35.7,
                departureLocation: departureCoords[i],
                arrivalLocation: arrivalCoords[i])
            )
        }
        
        /* Sample:
        allTrips.append(Trip(date: "ВЧЕРА, 14 АПРЕЛЯ, 2017", count: "3 ПОЕЗДКИ", cost: "501,30 ₽", distance: "140,9 км", time: "3 ч. 32 мин."))
        
        */
    }
    
    func returnTotal() -> (distance: Double, fuelCons: Double, lastingTime: Int, cost: Double) {
        
        var total: (Double, Double, Int, Double) = (0.0, 0.0, 0, 0.0)
        
        for index in 0..<self.allTrips.count {
            total.0 += self.allTrips[index].distance
            total.1 += Double(self.allTrips[index].fuelCons)
            total.2 += self.allTrips[index].lastingTime
            total.3 += Double(self.allTrips[index].cost)
        }
        
        return total
    }
    
    func removeTrip(_ item: Trip) {
        if let index = allTrips.index(of: item) {
            allTrips.remove(at: index)
        }
    }
    
}
