//
//  TripInfoViewController.swift
//  OBD
//
//  Created by Дмитрий Жаров on 02.05.17.
//  Copyright © 2017 Dmitriy Zharov. All rights reserved.
//

import UIKit
import MapKit
import AddressBookUI

class TripDetailsViewController: UIViewController, MKMapViewDelegate {
    
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var tripMapCardView: UIView!
    @IBOutlet var tripInfoCardView: UIView!
    @IBOutlet var tripAddressCardView: UIView!
    @IBOutlet var tripDeleteCardView: UIView!
    
    @IBOutlet var departureTimeLabel: UILabel!
    @IBOutlet var betweenTimeLabel: UILabel!
    
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var averageSpeedLabel: UILabel!
    
    @IBOutlet var costLabel: UILabel!
    @IBOutlet var costPer1Label: UILabel!
    
    
    @IBOutlet var fuelConsLabel: UILabel!
    @IBOutlet var fuelConsPer100Label: UILabel!
    
    @IBOutlet var roughAccelsLabel: UILabel!
    @IBOutlet var roughStopsLabel: UILabel!
    
    @IBOutlet var departureAddressLabel: UILabel!
    @IBOutlet var arrivalAddressLabel: UILabel!
    
    @IBAction func deleteTrip(_ sender: Any) {
        tripStore.allTrips.remove(at: tripIndex)
    }
    
    var trip: Trip!
    var tripIndex: Int!
    var tripStore: TripStore!
    
    var departureLocation: CLLocationCoordinate2D!
    var arrivalLocation: CLLocationCoordinate2D!
    
    let numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        return formatter
    }()
    
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, dd MMMM, yyyy"
        return formatter
    }()
        
    let timeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        return formatter
        }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        departureTimeLabel.text = dateFormatter.string(from: trip.departureTime).uppercased()
        betweenTimeLabel.text = timeFormatter.string(from: trip.departureTime) + " - " + timeFormatter.string(from: trip.arrivalTime)
        
        distanceLabel.text = numberFormatter.string(from: NSNumber(value: trip.distance))! + " км"
        averageSpeedLabel.text = numberFormatter.string(from: NSNumber(value: trip.averageSpeed))! + " км/ч"
        
        costLabel.text = numberFormatter.string(from: NSNumber(value: trip.cost))! + " ₽"
        costPer1Label.text = numberFormatter.string(from: NSNumber(value: trip.costPer1))! + " ₽"
        fuelConsLabel.text = numberFormatter.string(from: NSNumber(value: trip.fuelCons))! + " л."
        fuelConsPer100Label.text = numberFormatter.string(from: NSNumber(value: trip.fuelConsPer100))! + " л."
        
        roughAccelsLabel.text = "\(trip.roughAccels)"
        roughStopsLabel.text = "\(trip.roughStops)"
        
        var location: CLLocation
        
        location = CLLocation(latitude: departureLocation.latitude, longitude: departureLocation.longitude)
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) in
            if error != nil {
                print("Geocode failed with error: \(error!.localizedDescription)")
            }
            
            if placemarks!.count > 0 {
                let placemark = placemarks![0]
                let addressDictionary = placemark.addressDictionary
                let address = addressDictionary!["Street"]
                
                self.departureAddressLabel.text = "\(address!)"
            }
        })
        
        location = CLLocation(latitude: arrivalLocation.latitude, longitude: arrivalLocation.longitude)
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) in
            if error != nil {
                print("Geocode failed with error: \(error!.localizedDescription)")
            }
            
            if placemarks!.count > 0 {
                let placemark = placemarks![0]
                let addressDictionary = placemark.addressDictionary
                let address = addressDictionary!["Street"]
        
                self.arrivalAddressLabel.text = "\(address!)"
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        
        self.departureLocation = trip.departureLocation
        self.arrivalLocation = trip.arrivalLocation
        
        let departurePlacemark = MKPlacemark(coordinate: departureLocation, addressDictionary: nil)
        let arrivalPlacemark = MKPlacemark(coordinate: arrivalLocation, addressDictionary: nil)
        
        let departureMapItem = MKMapItem(placemark: departurePlacemark)
        let arrivalMapItem = MKMapItem(placemark: arrivalPlacemark)
        
        let departureAnnotation = MKPointAnnotation()
        departureAnnotation.title = "Пункт отправления"
        
        if let location = departurePlacemark.location {
            departureAnnotation.coordinate = location.coordinate
        }
        
        let arrivalAnnotation = MKPointAnnotation()
        arrivalAnnotation.title = "Пункт назначения"
        
        if let location = arrivalPlacemark.location {
            arrivalAnnotation.coordinate = location.coordinate
        }
        
        self.mapView.showAnnotations([departureAnnotation,arrivalAnnotation], animated: true )
        
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = departureMapItem
        directionRequest.destination = arrivalMapItem
        directionRequest.transportType = .automobile
        directionRequest.requestsAlternateRoutes = false
        
        let directions = MKDirections(request: directionRequest)
        
        directions.calculate(completionHandler: {(response, error) in
            if error != nil {
                print("Error getting directions")
            } else {
                self.showRoute(response!)
            }
        })
        
        tripMapCardView.layer.borderWidth = 0.5
        tripMapCardView.layer.borderColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:1.0).cgColor
        
        tripInfoCardView.layer.borderWidth = 0.5
        tripInfoCardView.layer.borderColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:1.0).cgColor
        
        tripAddressCardView.layer.borderWidth = 0.5
        tripAddressCardView.layer.borderColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:1.0).cgColor
        
        tripDeleteCardView.layer.borderWidth = 0.5
        tripDeleteCardView.layer.borderColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:1.0).cgColor
    }
    
    func showRoute(_ response: MKDirectionsResponse) {
        
        for route in response.routes {
            
            mapView.add(route.polyline, level: MKOverlayLevel.aboveRoads)

        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor(red:1.00, green:0.18, blue:0.33, alpha:1.0)
        renderer.lineWidth = 3.0
        return renderer
    }
}
