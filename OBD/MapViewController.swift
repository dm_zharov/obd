//
//  MapViewController.swift
//  OBD
//
//  Created by Дмитрий Жаров on 27.04.17.
//  Copyright © 2017 Dmitriy Zharov. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet var segmentedControlCardView: UIView!
    @IBOutlet var segmentedControl: UISegmentedControl!
    @IBOutlet var mapView: MKMapView!
    
    @IBAction func mapTypeChanged(_ sender: UISegmentedControl) {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            mapView.mapType = .standard
        case 1:
            mapView.mapType = .hybrid
        case 2:
            mapView.mapType = .satellite
        default:
            break
        }
    }

    @IBAction func mapScaleIncrease(_ sender: Any) {
        var region: MKCoordinateRegion = mapView.region
        var span: MKCoordinateSpan = mapView.region.span
        span.latitudeDelta *= 0.5
        span.longitudeDelta *= 0.5
        region.span = span
        mapView.setRegion(region, animated: true)
    }
    
    @IBAction func mapScaleDecrease(_ sender: Any) {
        var region: MKCoordinateRegion = mapView.region
        var span: MKCoordinateSpan = mapView.region.span
        span.latitudeDelta *= 2
        span.longitudeDelta *= 2
        region.span = span
        mapView.setRegion(region, animated: true)
    }
    
    var tripStore: TripStore!
    var locationManager: CLLocationManager!
    var userLocation: CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        mapView.showsUserLocation = true
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            //locationManager.startUpdatingHeading()
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) { userLocation = locations[0] as CLLocation
        // Drop a pin at cars's Current Location
        if tripStore.allTrips.count > 0 {
            let carAnnotation: MKPointAnnotation = MKPointAnnotation()
            carAnnotation.coordinate = tripStore.allTrips[0].arrivalLocation
            carAnnotation.title = "Chevrolet Cruze"
            mapView.addAnnotation(carAnnotation)
            mapView.selectAnnotation(mapView.annotations[0], animated: true)
            
            self.getDirection()
        } else {
            self.navigationItem.title = "Ваше местоположение"
        }
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        manager.stopUpdatingLocation()
        
        let center = CLLocationCoordinate2D(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        mapView.setRegion(region, animated: true)
        
        mapView.userLocation.title = "Ваше местоположение"
    }
    
    func getDirection() {
        
        let arrivalLocation = tripStore.allTrips[0].arrivalLocation
        let arrivalPlacemark = MKPlacemark(coordinate: arrivalLocation, addressDictionary: nil)
        let destination = MKMapItem(placemark: arrivalPlacemark)
        
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = MKMapItem.forCurrentLocation()
        directionRequest.destination = destination
        directionRequest.transportType = .walking
        directionRequest.requestsAlternateRoutes = false
        
        let direction = MKDirections(request: directionRequest)
        
        direction.calculate(completionHandler: {(response, error) in
            if error != nil {
                print("Error getting directions")
            } else {
                self.showRoute(response!)
            }
        })
    }
    
    func showRoute(_ response: MKDirectionsResponse) {
        
        for route in response.routes {
            
            mapView.add(route.polyline, level: MKOverlayLevel.aboveRoads)
    
        }
        
        let region = MKCoordinateRegionMakeWithDistance(userLocation!.coordinate, 2000, 2000)
        
        mapView.setRegion(region, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor
        overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        
        renderer.strokeColor = UIColor(red:1.00, green:0.18, blue:0.33, alpha:1.0)
        renderer.lineWidth = 3.0
        return renderer
    }
}
