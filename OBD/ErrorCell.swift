//
//  ErrorCell.swift
//  OBD
//
//  Created by Дмитрий Жаров on 03.05.17.
//  Copyright © 2017 Dmitriy Zharov. All rights reserved.
//

import UIKit

class ErrorCell: UITableViewCell {
    
    @IBOutlet var errorTitleLabel: UILabel!
    @IBOutlet var errorDescLabel: UILabel!
    
    @IBOutlet var cardView: UIView!
}
