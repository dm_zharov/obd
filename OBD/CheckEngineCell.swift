//
//  CheckEngineCell.swift
//  OBD
//
//  Created by Дмитрий Жаров on 03.05.17.
//  Copyright © 2017 Dmitriy Zharov. All rights reserved.
//

import UIKit

class CheckEngineCell: UITableViewCell {
    
    @IBOutlet var countLabel: UILabel!
    @IBOutlet var cardView: UIView!
    
}
