//
//  StatisticsCell.swift
//  OBD
//
//  Created by Дмитрий Жаров on 04.05.17.
//  Copyright © 2017 Dmitriy Zharov. All rights reserved.
//

import UIKit

class StatisticsCell: UITableViewCell {
    
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var fuelConsLabel: UILabel!
    @IBOutlet var lastingTimeLabel: UILabel!
    @IBOutlet var costLabel: UILabel!
    
}
