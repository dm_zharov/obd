//
//  DiagnosticsViewController.swift
//  OBD
//
//  Created by Дмитрий Жаров on 03.05.17.
//  Copyright © 2017 Dmitriy Zharov. All rights reserved.
//

import UIKit

class DiagnosticsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var errorStore: ErrorStore!
    
    @IBOutlet var countLabel: UILabel!
    @IBOutlet var cardView: UIView!
    @IBOutlet var tableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let count = errorStore.allErrors.count
        
        switch count {
        case 1:
            countLabel.text = "\(count) ОШИБКА"
        case 2,3,4:
            countLabel.text = "\(count) ОШИБКИ"
        default:
            countLabel.text = "\(count) ОШИБОК"
        }
        
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        //  let statusBarHeight = UIApplication.shared.statusBarFrame.height
        //let insets = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)
        //tableView.contentInset = insets
        //tableView.scrollIndicatorInsets = insets
        
        cardView.layer.borderWidth = 0.5
        cardView.layer.borderColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:1.0).cgColor
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return errorStore.allErrors.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Get a new or recycled cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "Error Cell", for: indexPath) as! ErrorCell
        
        // Set the text on the cell with the description of the item
        // that is at the nth index of items, where n = row this cell
        // will appear in on the tableview
        let error = errorStore.allErrors[indexPath.row]
        
        // Configure the cell with the Item
        cell.errorTitleLabel.text = error.errorTitle
        cell.errorDescLabel.text = error.errorDesc
        
        cell.cardView.layer.borderWidth = 0.5
        cell.cardView.layer.borderColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:1.0).cgColor
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showErrorDetails"?:
            // If the triggered segue is the "showTripDetails" segue
            if let row = tableView.indexPathForSelectedRow?.row {
                // Get the item associated with this row and pass it along
                let error = errorStore.allErrors[row]
                let errorDetailsViewController = segue.destination as! ErrorDetailsViewController
                
                errorDetailsViewController.error = error
                errorDetailsViewController.errorIndex = row
                errorDetailsViewController.errorStore = errorStore
            }
        default:
            preconditionFailure("Unexpected segue identifier.")
        }
    }
    
    @IBAction func unwindFromErrorDetailsView(segue: UIStoryboardSegue) {
        //nothing goes here
    }
    
}
