//
//  TripsViewController.swift
//  OBD
//
//  Created by Дмитрий Жаров on 30.04.17.
//  Copyright © 2017 Dmitriy Zharov. All rights reserved.
//

import UIKit
import MapKit

class TripsViewController: UITableViewController {
        
    var tripStore: TripStore!
    
    let numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        return formatter
        }()
    
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE, dd MMMM, yyyy"
        return formatter
        }()
    
    let relativeDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .none
        formatter.doesRelativeDateFormatting = true
        return formatter
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        //  let statusBarHeight = UIApplication.shared.statusBarFrame.height
        
        let insets = UIEdgeInsets(top: -12-300, left: 0, bottom: 0, right: 0)
        tableView.contentInset = insets
        // tableView.scrollIndicatorInsets = insets
        
        self.navigationItem.setTitle(title: "Chevrolet Cruze", subtitle: "Выпуск: 2011 г., модель: LT 1.8")
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + tripStore.allTrips.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            switch indexPath.row {
            case 0:
                // Get a new or recycled cell
                let cell = tableView.dequeueReusableCell(withIdentifier: "Statistics Cell", for: indexPath) as! StatisticsCell
                let total = tripStore.returnTotal()
                
                cell.distanceLabel.text = numberFormatter.string(from: NSNumber(value: total.distance))! + " км"
                cell.fuelConsLabel.text = numberFormatter.string(from: NSNumber(value: total.fuelCons))! + " л."
                cell.lastingTimeLabel.text = "\(total.lastingTime / 60)" + " ч. " + "\(total.lastingTime % 60)" + " мин."
                cell.costLabel.text = numberFormatter.string(from: NSNumber(value: total.cost))! + " ₽"
                
                return cell
            default:
                // Get a new or recycled cell
                let cell = tableView.dequeueReusableCell(withIdentifier: "Trip Cell", for: indexPath) as! TripCell
                
                // Set the text on the cell with the description of the item
                // that is at the nth index of items, where n = row this cell
                // will appear in on the tableview
                let trip = tripStore.allTrips[indexPath.row - 1]
                
                // Configure the cell with the Item
                cell.departureTimeLabel.text = dateFormatter.string(from: trip.departureTime).uppercased()
                cell.numberLabel.text = "\(trip.number) ПОЕЗДКА"
                cell.costLabel.text = numberFormatter.string(from: NSNumber(value: trip.cost))! + " ₽"
                cell.distanceLabel.text = numberFormatter.string(from: NSNumber(value: trip.distance))! + " км"
                cell.lastingTimeLabel.text = "\(trip.lastingTime / 60)" + " ч. " + "\(trip.lastingTime % 60)" + " мин."
                
                cell.departureLocation = trip.departureLocation
                cell.arrivalLocation = trip.arrivalLocation
                
                return cell
            }            
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 142+300
        default:
            return 271
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    //override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //    self.performSegue(withIdentifier: "toTripDetailsViewSegue", sender: self)
    //}
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showTripDetails"?:
            // If the triggered segue is the "showTripDetails" segue
            if let row = tableView.indexPathForSelectedRow?.row {
                // Get the item associated with this row and pass it along
                let trip = tripStore.allTrips[row - 1]
                let tripDetailsViewController = segue.destination as! TripDetailsViewController
                
                tripDetailsViewController.trip = trip
                tripDetailsViewController.tripIndex = row - 1
                tripDetailsViewController.tripStore = tripStore
            }
        default:
            preconditionFailure("Unexpected segue identifier.")
        }
    }
    
    @IBAction func unwindFromTripDetailsView(segue: UIStoryboardSegue) {
        //nothing goes here
    }
    
}

extension UINavigationItem {
    
    func setTitle(title:String, subtitle:String) {
        
        let one = UILabel()
        one.text = title
        one.font = UIFont.systemFont(ofSize: 17, weight: UIFontWeightSemibold)
        one.textColor = UIColor.white
        one.textAlignment = .center
        one.sizeToFit()
        
        let two = UILabel()
        two.text = subtitle
        two.font = UIFont.systemFont(ofSize: 11)
        two.textColor = UIColor.white
        two.textAlignment = .center
        two.sizeToFit()
        
        
        
        let stackView = UIStackView(arrangedSubviews: [one, two])
        stackView.distribution = .equalCentering
        stackView.axis = .vertical
        
        let width = max(one.frame.size.width, two.frame.size.width)
        stackView.frame = CGRect(x: 0, y: 0, width: width, height: 35)
        
        one.sizeToFit()
        two.sizeToFit()
        
        
        
        self.titleView = stackView
    }
    
}
