//
//  Trip.swift
//  OBD
//
//  Created by Дмитрий Жаров on 30.04.17.
//  Copyright © 2017 Dmitriy Zharov. All rights reserved.
//

import UIKit
import MapKit

class Trip: NSObject {
    // Departure and arrival time
    let departureTime: Date
    let arrivalTime: Date
    let lastingTime: Int
    
    // Total distance of trip
    let distance: Double
    
    // Average speed
    let averageSpeed: Double
    
    // Fuel consumption
    let fuelCons: Float //
    
    // Fuel consumption of 100 Km
    let fuelConsPer100: Float
    
    // Fuel price
    let fuelPrice: Float
    
    // Total cost of trip
    let cost: Float
    
    // 1 km cost of trip
    let costPer1: Float
    
    // Count of day trip
    let number: Int

    // Rough accelerations and stops
    let roughAccels: Int
    let roughStops: Int
    
    // Departure and arrivar location
    let departureLocation: CLLocationCoordinate2D
    let arrivalLocation: CLLocationCoordinate2D
        
    init(
        departureTime: Date!,
        arrivalTime: Date!,
        distance: Double!,
        fuelCons: Float!,
        fuelPrice: Float!,
        departureLocation: (latitude: Double, longitude: Double)!,
        arrivalLocation: (latitude: Double, longitude: Double)!
        ) {
        
        self.departureTime = departureTime
        self.arrivalTime = arrivalTime
        self.lastingTime = Int(arrivalTime.timeIntervalSince(departureTime)) / 60
        
        self.distance = distance / 1000.0
        self.averageSpeed = self.distance / (Double(self.lastingTime) / 60.0)
        
        self.fuelPrice = fuelPrice
        self.fuelCons = fuelCons
        self.fuelConsPer100 = Float((Double(self.fuelCons) / self.distance) * 100.0)
        self.costPer1 = Float((Double(self.fuelCons) / self.distance)) * self.fuelPrice
        self.cost = Float(Double(self.costPer1) * self.distance)
        
        self.number = 1
        self.roughAccels = Int(arc4random_uniform(UInt32(5)))
        self.roughStops = Int(arc4random_uniform(UInt32(5)))
        
        // "ул.Маковскго, 2"
        self.departureLocation = CLLocationCoordinate2D(latitude: departureLocation.0, longitude: departureLocation.1)
        
        // "ул.Таллинская, 34"
        self.arrivalLocation = CLLocationCoordinate2D(latitude: arrivalLocation.0, longitude: arrivalLocation.1)
        
        super.init()
    }
}
