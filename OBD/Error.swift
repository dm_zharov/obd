//
//  Error.swift
//  OBD
//
//  Created by Дмитрий Жаров on 03.05.17.
//  Copyright © 2017 Dmitriy Zharov. All rights reserved.
//

import UIKit

class Error: NSObject {
    let errorTitle: String!
    let errorDesc: String!
    let errorDate: String!
    
    init(errorTitle: String!, errorDesc: String!, errorDate: String!) {
        self.errorTitle = errorTitle
        self.errorDesc = errorDesc
        self.errorDate = errorDate
        
        super.init()
    }
}
