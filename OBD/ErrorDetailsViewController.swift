//
//  ErrorDetailsViewController.swift
//  OBD
//
//  Created by Дмитрий Жаров on 03.05.17.
//  Copyright © 2017 Dmitriy Zharov. All rights reserved.
//

import UIKit

class ErrorDetailsViewController: UIViewController {
    
    var error: Error!
    var errorIndex: Int!
    var errorStore: ErrorStore!
    
    @IBOutlet var errorTitle: UILabel!
    @IBOutlet var errorDesc: UILabel!
    @IBOutlet var errorDate: UILabel!
    
    @IBOutlet var errorCardView: UIView!
    @IBOutlet var descCardView: UIView!
    @IBOutlet var deleteCardView: UIView!
    
    @IBAction func deleteError(_ sender: Any) {
        errorStore.allErrors.remove(at: errorIndex)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        errorTitle.text = error.errorTitle
        errorDesc.text = error.errorDesc
        errorDate.text = error.errorDate
        
        //valueField.text = numberFormatter.string(from: NSNumber(value: item.valueInDollars))
        //dateLabel.text = dateFormatter.string(from: item.dateCreated)
    }
    
    override func viewDidLoad() {
        // let statusBarHeight = UIApplication.shared.statusBarFrame.height
        // let insets = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)
        // tableView.contentInset = insets
        // tableView.scrollIndicatorInsets = insets
        
        errorCardView.layer.borderWidth = 0.5
        errorCardView.layer.borderColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:1.0).cgColor
        
        descCardView.layer.borderWidth = 0.5
        descCardView.layer.borderColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:1.0).cgColor
    }
    
}
