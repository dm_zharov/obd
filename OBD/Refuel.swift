//
//  Refuel.swift
//  OBD
//
//  Created by Дмитрий Жаров on 03.05.17.
//  Copyright © 2017 Dmitriy Zharov. All rights reserved.
//

import UIKit

class Refuel: NSObject {
    let refuelDate: String!
    
    init(refuelDate: String!) {
        self.refuelDate = refuelDate
        
        super.init()
    }
}
