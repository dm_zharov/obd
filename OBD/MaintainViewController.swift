//
//  ViewController.swift
//  OBD
//
//  Created by Дмитрий Жаров on 27.04.17.
//  Copyright © 2017 Dmitriy Zharov. All rights reserved.
//

import UIKit

class MaintainViewController: UITableViewController {
    
    var errorStore: ErrorStore!
    var refuelStore: RefuelStore!
    
    override func viewDidLoad() {
        //  let statusBarHeight = UIApplication.shared.statusBarFrame.height
        
        let insets = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)
        tableView.contentInset = insets
        tableView.scrollIndicatorInsets = insets
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows = 0
        if errorStore.allErrors.count > 0 {
            numberOfRows += 1
        }
        if refuelStore.allRefuels.count > 0 {
            numberOfRows += 1
        }
        
        return numberOfRows
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            // Get a new or recycled cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "Check Engine Cell", for: indexPath) as! CheckEngineCell
            
            // Set the text on the cell with the description of the item
            // that is at the nth index of items, where n = row this cell
            // will appear in on the tableview
            let count = errorStore.allErrors.count
            
            // Configure the cell with the Item
            switch count {
            case 1:
                cell.countLabel.text = "\(count) ОШИБКА"
            case 2,3,4:
                cell.countLabel.text = "\(count) ОШИБКИ"
            default:
                cell.countLabel.text = "\(count) ОШИБОК"
            }
            
            cell.cardView.layer.borderWidth = 0.5
            cell.cardView.layer.borderColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:1.0).cgColor
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Refuel Cell", for: indexPath) as! RefuelCell
            
            // Set the text on the cell with the description of the item
            // that is at the nth index of items, where n = row this cell
            // will appear in on the tableview
            let count = refuelStore.allRefuels.count
            
            // Configure the cell with the Item
            switch count {
            case 1:
                cell.countLabel.text = "\(count) ЗАПРАВКА"
            case 2,3,4:
                cell.countLabel.text = "\(count) ЗАПРАВКИ"
            default:
                cell.countLabel.text = "\(count) ЗАПРАВОК"
            }
            
            cell.cardView.layer.borderWidth = 0.5
            cell.cardView.layer.borderColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:1.0).cgColor
            
            return cell
        default:
            preconditionFailure("Unexpected segue identifier.")

        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 201
        case 1:
            return 171
        default:
            return 201
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showDiagnostics"?:
            // If the triggered segue is the "showErrors" segue
            //if let row = tableView.indexPathForSelectedRow?.row {
                // Get the item associated with this row and pass it along
                let diagnosticsViewController = segue.destination as! DiagnosticsViewController
                diagnosticsViewController.errorStore = errorStore
            //}
        default:
            preconditionFailure("Unexpected segue identifier.")
        }
    }
}

